import React from 'react'
import { Container } from 'react-bootstrap'

export const Hello = () => {

    return (
        <Container fluid>
            Welcome
        </Container>
    )
}
