# Plataforma CIMM
Plataforma construida con React para el proyecto de Catalogación Inteligente Maestro de Materiales

### Estructura

- **src** 				=> Carpeta principal con el código fuente.
	- **assets** 		=> Imagenes, fuentes, estilos.
	- **components** 	=> Componentes reutilizables en las vistas del proyecto.
	- **core** 			=> servicios, utilidades, funciones, para el manejo de 		
						   información dentro de la plataforma.
	- **views** 		=> Vistas principales dentro de la interfaz.

### Requisitos

- Node.js
- npm

### Ejecución 

Local:
```
npm start
```

Al iniciar la aplicación ingresar las siguientes credenciales de prueba:

- Usuario: eduardo@gestalabs.com
- Contraseña: Temp1234

### Deploy

```
az login
az acr build --image platform/crmtromatdev2:dev --registry crmtromatdev2 --file Dockerfile .

az acr build --image desktopapp/proleclab:lab --registry PLStreamingFrontendIoTEdgeModule --file Dockerfile .
```

####### Pipeline V1.1

