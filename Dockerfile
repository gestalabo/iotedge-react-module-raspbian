# Stage 0: Build React APP
FROM arm32v7/node:10-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
RUN npm install react-scripts@3.3.0 -g
# RUN npm audit fix
COPY . ./
RUN npm run build

# Stage 1: Expose with NGINX
FROM arm32v7/nginx:1.20.1
WORKDIR /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /usr/share/nginx/html
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]